#!/bin/bash

echo Disk is $1 , Root is "$1"1
read -rsp $'Press enter to continue...\n'
echo Updating system clock
timedatectl set-ntp true
echo Partitioning Disk
fdisk /dev/$1
echo Formating the partitions
mkfs.ext4 /dev/"$1"1
echo Mounting the file systems
mount /dev/sda1 /mnt
echo Creating swap file
fallocate -l 1G /mnt/swapfile
chmod 600 /mnt/swapfile
mkswap /mnt/swapfile
swapon /mnt/swapfile
echo Configuring the repository and installing packages
nano /etc/pacman.conf
pacman -Syy
pacman -S reflector
reflector --verbose --country Romania --country Germany --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
echo Installing Arch Linux
pacstrap -i /mnt base base-devel git grub
echo Configuring fstab
genfstab -U /mnt >> /mnt/etc/fstab
echo DONE Now do arch-chroot /mnt
