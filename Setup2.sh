#!/bin/bash

echo Disk is $1
read -rsp $'Press enter to continue...\n'
echo Setting up time zone
ln -sf /usr/share/zoneinfo/Europe/Bucharest /etc/localtime
hwclock --systohc
echo Configuring language and location
nano /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 >> /etc/locale.conf
echo Setting up hostname and network
echo arch >> /etc/hostname
echo "127.0.0.1        localhost" >> /etc/hosts
echo "::1              localhost" >> /etc/hosts
echo "127.0.1.1        arch.localdomain	arch" >> /etc/hosts
nano /etc/hosts
systemctl enable dhcpcd.service
echo Setting up root password and creating new user
passwd
useradd -m -G adm,wheel marius
passwd marius
echo Setting up sudo
EDITOR=nano visudo
echo Installing and configuring grub
grub-install --target=i386-pc /dev/$1
grub-mkconfig -o /boot/grub/grub.cfg
